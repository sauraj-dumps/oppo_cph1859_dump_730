## lineage_CPH1859-userdebug 11 RQ3A.210905.001 eng.kardeb.20210923.115228 test-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1859
- Brand: oppo
- Flavor: lineage_CPH1859-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.kardeb.20210923.115228
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/lineage_CPH1859/CPH1859:11/RQ3A.210905.001/kardebayan309231149:userdebug/test-keys
- OTA version: 
- Branch: lineage_CPH1859-userdebug-11-RQ3A.210905.001-eng.kardeb.20210923.115228-test-keys
- Repo: oppo_cph1859_dump_730


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
